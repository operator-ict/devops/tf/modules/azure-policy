locals {
  def_reference = [
    for id, p in var.policies : {
      id = "/providers/Microsoft.Authorization/policyDefinitions/${id}",
      parameters = jsonencode({
        for key, value in p : key => {"value": "[parameters('${key}')]"}
      })
    }
  ]
  val_str = { for key,val in var.parameters : key => {
    "value" = val.type == "String" ? val.value[0] : null
    } if val.type == "String"
  }
  val_arr = { for key,val in var.parameters : key => {
    "value" = val.type != "String" ? val.value : null
    } if val.type != "String"
  }
  val_mer = merge(local.val_str,local.val_arr)
  val_json = jsonencode(local.val_mer)
  def = jsonencode({
    for key,val in var.parameters : key => {
         "metadata" = val.metadata
         "type" = val.type
    }
  })
}

resource "azurerm_policy_set_definition" "pst" {
  name         = var.name
  policy_type  = "Custom"
  display_name = var.name
  parameters   = local.def
  management_group_id = var.management_group_id

  dynamic "policy_definition_reference" {
      for_each =  local.def_reference
      content {
        policy_definition_id = policy_definition_reference.value["id"]
        parameter_values = policy_definition_reference.value["parameters"]
      }
  }

  policy_definition_group {
    name         = var.group_name
    description  = var.group_description
    category     = var.group_category
    display_name = var.group_name
  }

}

resource "azurerm_subscription_policy_assignment" "spa" {
  for_each = var.subscriptions_id
  name                 = "${var.name} ${each.key}"
  policy_definition_id = azurerm_policy_set_definition.pst.id
  subscription_id      = "/subscriptions/${each.value}"
  parameters           = local.val_json
}
