variable "name" {
  type = string
}

variable "group_name" {
  type = string
}

variable "group_category" {
  type = string
}

variable "group_description" {
  type = string
}

variable "management_group_id" {
  type = string
}

variable "policies" {
  type = map(any)
}

variable "parameters" {
  type = map(object({
    type = string
    metadata = object({
      displayName = string
      description = string
      portalReview = bool
    })
    value = list(string)
  }))
}

variable "subscriptions_id" {
  type = map(any)
}
